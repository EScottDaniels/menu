// :vi ts=4 sw=4 noet:

/*
	Mnemonic:	menu.go
	Abstract:	Struct and functions to manage a single set of menu buttons.
	Date:		12 December 2018
	Author:		E. Scott Daniels

	Notes:		Buttons are automatically drawn into the double buffer window before
				the buffer is swapped.
				Subpage background colour is automatically set, and the sp cleared
				with each call to the page's show().
				All visible menu subpages should be painted without special call
				to any function here.
*/
package menu

import (
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)

/*
	Manage a single menu with one anchor/control button.
*/
type menu struct {
	gc	sketch.Graphics_api		// the graphics context api
	name string					// the name of the menu (used for subpage name)
	anchor	*button				// the button that opens/closes the menu
	buttons []*button			// buttons which belong to the menu
	echan chan *Mevent			// channel where user is listening for callback events
	ichan chan *sktools.Interaction			// our listener channel
	visible		bool						// true if menu is currently on screen

	x		float64				// locaiton of anchor button; subpage position based on this too
	y		float64
	height	float64				// dimensions of buttons in the window
	width	float64
	page_colour string			// page colour (usually slightly darker than buttons)
}

/*
	Display causes a menu subpage to be displayed.
*/
func ( m *menu ) display(  ) {
	if m == nil || m.visible {
		return 
	}

	gc := m.gc

	gc.Select_subpage( "root" )		// future:  allow subpage on subpage
	oheight := (len( m.buttons )  *  int( m.height + 3.0 )) + 3
	gc.Mk_locked_subpage( m.name,  10.0,  m.height + 5.0, float64( oheight ), m.width + 16.0 )  // buttons are a fixed 8 from each side

	gc.Select_subpage( "root" )
	m.gc.Select_subpage( m.name )
	m.gc.Clear_subpage( m.page_colour )

	y := float64( 4.0 )
	for i := 0; i < len( m.buttons ); i++ {
		b := m.buttons[i]
		gc.Add_button( m.buttons[i].bid, 8, int( y ), int( m.height ), int( m.width ), b.text, b.colour, b.txt_colour, b.kind, b.is_in )
		y += m.height + 3.0
	}

	m.gc.Select_subpage( m.name )
	m.gc.Add_listener( m.ichan )
	m.gc.Select_subpage( "root" )

	m.visible = true
}

/*
	Hide causes the menu in a subwindow to be removed.
*/
func ( m *menu ) hide(  ) {
	if m == nil || !m.visible {
		return
	}

	m.gc.Select_subpage( "root" )
	m.gc.Delete_subpage( m.name )

	m.visible = false
}

/*
	Toggle_visibility will change the menu's visibility to the opposite
	of what it is currently.
*/
func ( m *menu ) toggle_visibility( ) {
	if m == nil {
		return
	}

	if m.visible {
		m.hide( )
	} else {
		m.display( ) 
	}
}
