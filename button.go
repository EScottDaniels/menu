// :vi sw=4 ts=4 noet:

/*
	Mnemonic:	button.go
	Abstract:	Support for an individual menu button.
	Date:		12 December 2018
	Author:		E. Scott Daniels
*/
package menu


/*
	Describes a menu button.
*/
type button struct {
	bid	int					// id
	kind int				// sticky, toggle, etc
	colour string			// button colour
	txt_colour	string		// text colour
	text string				// string painted on the button
	is_in bool				// true if the button is 'depressed'
	uchan	chan *Mevent	// user's channel to handle events for this button
}

func mk_button( bid int, kind int, label string, colour string, txt_colour string, pressed bool, uchan chan *Mevent ) ( *button ) {
	b := &button {
		bid:	bid,
		kind:	kind,
		colour:	colour,
		txt_colour: txt_colour,
		text:	label,
		is_in:	pressed,
		uchan:	uchan,
	}

	return b
}
