module bitbucket.org/EScottDaniels/menu

go 1.15

require (
	bitbucket.org/EScottDaniels/colour v1.2.0
	bitbucket.org/EScottDaniels/sketch v1.2.0
)
