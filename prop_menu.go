// :vi ts=4 sw=4 noet:

/*
	Mnemonic:	prop_menu.go
	Abstract:	A "property" and related menu manger. Properties are binary (true/false).
				The user programme must establish and initialse the sketch xigo environment
				and supply the graphcs context at initialisation. The user programme is also
				expected to execute the Drive() function of the gc in order to drive the
				event manager.
	Date:		16 December 2018
	Author:		E. Scott Daniels

*/
package menu

import (
	"fmt"
	"encoding/json"
	"os"

	"bitbucket.org/EScottDaniels/sketch"
	//sktools "bitbucket.org/EScottDaniels/sketch/tools"
	colour "bitbucket.org/EScottDaniels/colour"
)

/*
	Manage a set of properties.
*/
type Properties struct {
	mm		*Menu_mgr		// the menu manger which has our menu
	mname string				// the name of the menu (used for subpage name)
	echan chan *Mevent			// channel where user is listening for callback events
	nbuttons	int				// number of buttons in the menu

	prop	map[string]bool		// list of properties
	b2prop	map[int]string		// map button id to property name
	toggle	map[int]bool		// toggle setting action (if false hide action assumed)
}

/*
	Structs to bash json definition into
*/
type Default_entry struct {		// defaults allowing an entry to inherit common settings
	Colour	string
	Kind	string
	Action	string				// close or set
}

type Prop_entry struct {
	Colour	string				// button colour for this setting
	Kind	string				// button kind (should be sticky except for the close button)
	Name	string				// property name Used to lookup
	Label	string				// button label
	Action	string				// button action (set causes value toggle, close causes menu hide)
	Init_state bool				// initial state (true == 'depressed')
}

type Definition struct {
	Defaults	Default_entry
	Entries		[]Prop_entry
}

/*
	Create a property menu. The menu is NOT anchored, and thus the user programme must cause it to be
	displayed directly (probably tied to a button click in an anchored menu).
*/
func Mk_prop_menu( mm *Menu_mgr, name string, x float64, y float64, height float64, width float64, def string ) ( p *Properties ) {
	if mm == nil {
		return
	}

	pm_def := &Definition{							// set our defaults; user definition will possibly overlay
		Defaults: Default_entry {
			Colour:	"#808080",
			Kind:	"sticky",
			Action:	"set",
		},
	}
		
	err := json.Unmarshal( []byte( def ), pm_def )
	if err != nil {
		fmt.Fprintf( os.Stderr, "prop_menu: unable to parse menu definition json for %s: %s\n", name, err )
		return
	}


	p = &Properties {
		mm:		mm,
		echan:	make( chan *Mevent, 128 ),
		prop:	make( map[string]bool, 64 ),
		b2prop:	make( map[int]string, 64 ),
		toggle:	make( map[int]bool, 64 ),
		mname:	name,
	}

	pc := colour.Mk_colour( pm_def.Defaults.Colour )			// darken the default colour from json
	pc.Darken( .15 )
	mm.Add_nab_menu( name, x, y, height, width, pc.String(), p.echan )

	p.nbuttons = len( pm_def.Entries )
	for i := 0; i < len( pm_def.Entries ); i++ {			// build a button for each entry and capture in map
		pe := pm_def.Entries[i]

		colour := pe.Colour
		if colour == "" {
			colour = pm_def.Defaults.Colour
		}
		action := pe.Action
		if action == "" {
			action = pm_def.Defaults.Action
		}
		kind := pe.Kind
		if kind == "" {
			kind = pm_def.Defaults.Kind
		}

		bkind := sketch.BUTTON_STICKY
		switch kind {
			case "spring":
				bkind =  sketch.BUTTON_SPRING

			case "radio":
				bkind =  sketch.BUTTON_RADIO
		}

		pressed := false
		if pe.Init_state {
			pressed = true
		}

		if pe.Name != "" {
			bid := mm.Add_button( name, pe.Label, colour, "white", bkind, pressed, p.echan )
			p.prop[pe.Name] = pressed				// use initial state if given; match button pressed
			p.b2prop[bid] = pe.Name
			p.toggle[bid] = (action == "set")
		}
	}

	go p.monitor( mm )				// start the channel listener
	return p
}

/*
	Is_set will return true of the named property is set.
*/
func ( p *Properties ) Is_set( pname string ) ( bool ) {
	if p == nil {
		return false
	}

	return p.prop[pname]
}

/*
	Completely destroy the menu.
*/
func (p *Properties ) Destroy( ) {
	if p == nil {
		return
	}
	
	p.mm.Hide_menu( p.mname )
	p.echan <- nil				// nil event signals the listener to stop.

}


/*
	Monitor listens for events on the channel and updates the property struct 
	accordingly. If a nil event is received, the function returns.
*/
func (p *Properties ) monitor( mm *Menu_mgr ) {
	if p == nil {
		return
	}

	for ;; {
		e := <- p.echan
		if e == nil {
			return
		}

		bid := e.Bid
		if p.toggle[bid] {				// if this is a sticky button that toggles state
			pname := p.b2prop[bid]
			p.prop[pname] = e.Is_in			
		} else {
			mm.Hide_menu( p.mname )			// if it's not a property button, assume it's a closer and shut us off
		}
	}
}
