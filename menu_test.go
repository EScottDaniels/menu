/*
	Mnemonic:	menu_test.go
	Abstract:	Basic unit test stuff.
	Author:		E. Scott Daniels
	Date:		13 December 2018
*/

package menu_test

import (
	"fmt"
	"os"
	"strings"
	"time"
	"testing"

	"bitbucket.org/EScottDaniels/sketch"
	"bitbucket.org/EScottDaniels/menu"
)

/*
	One instance of this function is started per menu that is created.
	This simplifies the code such that only the base of the button IDs
	is needed.
*/
func event_leach( mm *menu.Menu_mgr, mname string, echan chan *menu.Mevent, base int, close_on_click bool ) {
	for ;; {
		e := <- echan
		bid := e.Bid - base				// relative button in menu

		switch mname {
			case "control":
				switch bid {
					case 0:		mm.Hide_menu( mname )

					case 1:		mm.Display_menu( "properties" )

					default:
							os.Exit( 0 )
				}
		}

		fmt.Fprintf( os.Stderr, "menu %s: bid=%d bval=%d\n", mname, e.Bid, bid )

		if close_on_click {
			mm.Hide_menu( mname )
		} else {
			if bid == 0 {
				mm.Hide_menu( mname )
			}
		}
	}
}


/*
	Propetry leach listens to an event channel for property menu events.
*/
func prop_leach( mm *menu.Menu_mgr, mname string, echan chan *menu.Mevent, base int, nbuttons int ) {
	for ;; {
		e := <- echan
		bid := e.Bid - base				// relative button in menu
		state := "unselected"
		if e.Is_in {
			state = "selected"
		}
		
		if bid > 0 {
			fmt.Fprintf( os.Stderr, "prop-leach%s: bid=%d bval=%d state=%s\n", mname, e.Bid, bid, state )
		} else {
			mm.Hide_menu( mname )
		}

	}
}

/*
	Given a list of button info tokens (label:colour:kind) create a menu. Returns
	the base button id and the number of buttons in the menu.
*/
func build_menu( mm *menu.Menu_mgr, name string, label string, info string, echan chan *menu.Mevent, anchored bool ) ( base int, nbuttons int ) {

	tokens := strings.Split( info, "," )

	if anchored {
		mm.Add_anchor( name, label, 20, 128, "#000060", echan )
	} else {
		mm.Add_nab_menu( name, 100, 100, 25, 130, "#000060", echan )
	}

	def_kind := sketch.BUTTON_SPRING				// default values should  extra info be missing
	def_colour := "#000080"
	for i := 0; i < len( tokens ); i++  {
		kind := def_kind
		colour := def_colour

		lc := strings.Split( tokens[i], ":" )
		if len( lc ) > 1 {
			colour = lc[1]

			if len( lc ) > 2 {
				switch lc[2] {
					case "sticky":
						kind = sketch.BUTTON_STICKY
	
					case "radio":
						kind = sketch.BUTTON_RADIO
				}
			}
		}

		bid := mm.Add_button( name, lc[0], colour, "white", kind, false, echan )
		if i == 0 {
			base = bid
		}
	}

	return base, len( tokens )
}

/*
*/
func Test_menu(t *testing.T) {
	var (
		m1_strs string = " Close Menu:#a0a000, Properties, Quit:#a00040"
		mp_strs string = " Close Menu:#a0a000, Long Refresh:#000080:sticky,Sound:#000080:sticky"
		m2_strs string = " m2 choice2, m2 choice 2, m2 choice 3, m2 choice 4, m2 choice 5"
		m3_strs string = " Close Menu, m3 choice 2, m3 choice 3, m3 choice 4, m3 choice 5, m3 choice 6, m3 choice 7, m3 choice 8"
	)

	gc, err := sketch.Mk_graphics_api( "xi", ",900,1200,Menu Test")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[FAIL] cannot create graphics conext: %s\n", err)
		t.Fail()
		return
	}
	fmt.Fprintf(os.Stderr, "[OK]  created graphics construct\n" );

	echan1 := make( chan *menu.Mevent, 128 )
	echan2 := make( chan *menu.Mevent, 128 )
	echan3 := make( chan *menu.Mevent, 128 )
	echanp := make( chan *menu.Mevent, 128 )

	gc.Select_subpage( "root" )
	m := menu.Mk_menu_set( gc, 10, 10, 25, 50, "#006060", "white", false )

	//labels := strings.Split( m1_strs, "," )
	//base_m1 := m.Add_menu( "m1", "List-7", 30, 128, "#00a0b0", "black", labels, echan1 )
	base_m1, _ := build_menu( m, "control", "Control", m1_strs, echan1, true )
	go event_leach( m, "control", echan1, base_m1, true )

	base_p, nbuttons := build_menu( m, "properties", "", mp_strs, echanp, false )
	go prop_leach( m, "properties", echanp, base_p, nbuttons )

	labels := strings.Split( m2_strs, "," )
	base_m2 := m.Add_menu( "m2", "List-5", 30, 128, "#008000", "white", labels, echan2 )
	go event_leach( m, "m2", echan2, base_m2, true )

	// third menu is defined using granular functions; more work == more control
	labels = strings.Split( m3_strs, "," )
	m.Add_anchor( "m3", "List-8", 20, 128, "#000060", echan3 )
	bcolour := "#a00040"
	base_m3 := 0
	kind := sketch.BUTTON_SPRING				// first button is spring, all others radio
	for i := 0; i < len( labels ); i++  {
		bid := m.Add_button( "m3", labels[i], bcolour, "white", kind, false, echan3 )
		if i == 0 {
			base_m3 = bid
		}
		bcolour = "#000080"
		kind = sketch.BUTTON_RADIO
	}
	go event_leach( m, "m3", echan3, base_m3, false )

	for ;; {
		gc.Show()
		gc.Drive()
		time.Sleep( 10 * time.Millisecond )
	}
}
