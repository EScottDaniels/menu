// : vi ts=4 sw=4 noet :

/*
	Mnemonic:	menu_mgr.go
	Abstract:	The menu manger is the master struct and functions that manages all
				defined menus, and provides the API that the user programme invokes.

	Date:		12 December 2018
	Author:		E. Scott Daniels


	Notes:		Buttons are automatically drawn into the double buffer window before
				the buffer is swapped.
				Subpage background colour is automatically set, and the sp cleared
				with each call to the page's show().
				All visible menu subpages should be painted without special call
				to any function here.
*/

/*
	Menu provides a sketch API based simple menu, button and mouse event event
	interface. 
*/
package menu

import (
	"fmt"
	"os"

	"bitbucket.org/EScottDaniels/colour"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
)



// -------------------------------------------------------------------------------------------

/*
	Menu event sent to to user on their channel.
*/
type Mevent struct {
	Bid	int						// button ID affected
	Is_in bool					// button state - true if button is 'depressed'
}

/*
	Menu manager, manages a set of menus. 
*/
type Menu_mgr struct {
	gc	sketch.Graphics_api
	ok2run bool					// event handler will stop if this goes false
	a2menu	map[int]*menu		// anchor button id to menu map
	nm2menu	map[string]*menu	// allow menu refrence by name
	b2menu	map[int]*menu		// must map button id to menu to pop radio buttons
	buttons map[int]*button		// master button list to determine echan from bid
	bid_nxt	int					// button id of the next button added
	abid_nxt int				// button id of next anchor
	align_vert	bool;			// vertical or horiz alignment of anchor buttons
	lchan	chan *sktools.Interaction	// channel that our listener waits on
	def_colour	string			// default colour for anchor buttons
	def_tcolour	string			// defautl text colour for anchor buttons

	x	float64					// anchor point and button dimensions
	y	float64
	height float64
	width float64
}


/*
	Mk_menu_set will create a menu system for the currently active subpage in gc. 
	User programme must first create an Xigo sketch graphics context (gc) which
	will also be used by the menus.

*/
func Mk_menu_set( gc sketch.Graphics_api, x float64, y float64, height float64, width float64, colour string, 
		txt_colour string, align_vert bool ) ( *Menu_mgr ) {
	mm := &Menu_mgr{
		gc:			gc,
		a2menu:		make( map[int]*menu, 17 ),
		b2menu:		make( map[int]*menu, 17 ),
		nm2menu:	make( map[string]*menu, 17 ),
		buttons:	make( map[int]*button, 1023 ),
		ok2run:		true,
		align_vert:	align_vert,
		def_colour:	colour,
		def_tcolour: txt_colour,
		x:			x,						// base x,y of the anchor buttons
		y:			y,
		height:		height,					// height/width of the anchor buttons
		width:		width,
		lchan:		make( chan *sktools.Interaction, 128 ),
		bid_nxt:	1025,					// anchor button ids <= 1204
	}

	go mm.event_leach()
	gc.Add_listener( mm.lchan )

	return mm
}

/*
	Add_anchor allows the user programme to add an anchored menu with out automatially adding any 
	buttons to the menu. Buttons can be added later allowing each to be coloured differently, or to 
	have different properties.
*/
func ( mm *Menu_mgr ) Add_anchor( name string, label string, height float64, width float64, page_colour string, uchan chan *Mevent ) ( bid int ) {
	if mm == nil {
		fmt.Fprintf( os.Stderr, "menu-mgr: add anchor: stuct refrerence is nil\n" )
		return
	}

	gc := mm.gc

	ax := int( mm.x )
	ay := int( mm.y )
	if mm.align_vert { // place the sub-window under the button
		ay += ( int(mm.height) * mm.abid_nxt) + (2 * mm.abid_nxt )
	} else { // place the sub-windo to the right of the button
		ax += (int(mm.width) * mm.abid_nxt) + (2 * mm.abid_nxt )
	}

	m := &menu {
		gc:		mm.gc,
		name:	name,
		height: height,
		width:	width,
		page_colour:	page_colour,
		buttons:	make( []*button, 0, 16 ),		// small initial capacity, but we append so it grows as needed
		ichan:	mm.lchan,							// our listener
		x:		float64( ax ),						// we'll place the subpage near this
		y:		float64( ay ),
	}

	gc.Add_button( mm.abid_nxt, int( ax ), int( ay ), int( mm.height ), int( mm.width ), label, mm.def_colour, mm.def_tcolour, sketch.BUTTON_SPRING, false );
	mm.a2menu[mm.abid_nxt] = m
	mm.nm2menu[name] = m
	bid = mm.abid_nxt
	mm.abid_nxt++

	return bid
}

/*
	Add_nab_menu adds a menu which has no anchor button. The user is responsible for causing the
	menu to be opened and closed. The x,y is used as the location to place the menu when displayed.
*/
func ( mm *Menu_mgr ) Add_nab_menu( name string, x float64, y float64, height float64, width float64, page_colour string, uchan chan *Mevent )  {
	if mm == nil {
		return
	}

	m := &menu {
		gc:		mm.gc,
		name:	name,
		height: height,
		width:	width,
		page_colour:	page_colour,
		buttons:	make( []*button, 0, 16 ),		// small initial capacity, but we append so it grows as needed
		ichan:	mm.lchan,							// our listener
		x:		x,									// we'll place the subpage near this
		y:		y,
	}

	mm.nm2menu[name] = m
}

/*
	Add_button allows a single button to be added to a menu. The button ID is automatically
	assigned, but user control over colour and type is allowed. Button height and width are taken
	from the menu definition set when the anchor was created.
*/
func ( mm *Menu_mgr ) Add_button( menu_name string, label string, colour string, txt_colour string, kind int, pressed bool, uchan chan *Mevent ) ( bid int ) {
	if mm == nil {
		return -1
	}

	m := mm.nm2menu[menu_name]
	if m == nil {
		fmt.Fprintf( os.Stderr, "menu_mgr: cannot find menu name: %s\n", menu_name )
		return -1
	}

	bid = mm.bid_nxt
	mm.bid_nxt++

	b := mk_button( bid, kind, label, colour, txt_colour, pressed, uchan )
	m.buttons = append( m.buttons, b )
	mm.buttons[bid] = b
	mm.b2menu[bid] = m

	return bid
}

/*
	Add_menu provides a 'one stop shopping' experience allowing the user programme to 
	add a complete menu to the mix.  Menu button labels are expected as an array of
	strings, button IDs are sequentially assigned, and button colours are all the same.
	Further, all buttons created "point" at the same user menu event channel.
	In the end, the user has less control, but the overall code in the user programme
	is likely more simple.
*/
func ( mm *Menu_mgr ) Add_menu( name string, anchor_label string, height float64, width float64, 
			bcolour string, txt_colour string, labels []string, uchan chan *Mevent ) ( start_bid int ) {

	if mm == nil {
		return
	}

	pc := colour.Mk_colour( bcolour )	// menu subpage backgrouund is 15% darker than button surface
	pc.Darken( .15 )
	abid := mm.Add_anchor( name, anchor_label, height, width, pc.String(), uchan )

	m := mm.a2menu[abid]
	start := mm.bid_nxt								// starting button ID to return
	for i := 0; i < len( labels ); i++ {			// add buttons
		b := mk_button( mm.bid_nxt, sketch.BUTTON_SPRING, labels[i], bcolour, txt_colour, false, uchan )
		m.buttons = append( m.buttons, b )
		mm.buttons[mm.bid_nxt] = b					// master list by button-id so leach knows what echan to tickle
		mm.b2menu[mm.bid_nxt] = m					// map button to owning menu
		mm.bid_nxt++
	}

	return start
}

/*
	Display menu causes the named menu's display state to be turned on.
	If it is already visible, no change is made.
*/
func ( mm *Menu_mgr ) Display_menu( name string ) {
	if mm == nil {
		return
	}

	m := mm.nm2menu[name]
	if m == nil {
		return
	}	

	m.display( )
}

/*
	Hide menu causes the named menu's display state to be turned off.
	If it is already hidden, no change is made.
*/
func ( mm *Menu_mgr ) Hide_menu( name string ) {
	if mm == nil {
		return
	}

	m := mm.nm2menu[name]
	if m == nil {
		return
	}	

	m.hide( )
}

/*
	Toggle menu causes the named menu's display state to be flipped.
*/
func ( mm *Menu_mgr ) Toggle_menu( name string ) {
	if mm == nil {
		return
	}

	m := mm.nm2menu[name]
	if m == nil {
		return
	}	

	m.toggle_visibility( )
}


/*
	Event_leach is an event handler that is 'inserted' between the X event generator
	(sketch) and the user application event handler.  We allow most events to percolate
	up, but this is responsible for opening/closing menus when a root button is clicked.
*/
func ( mm *Menu_mgr ) event_leach( ) {

	pressed_on := 0							// button id where press occurred; release dropped if different
	for ; mm.ok2run; {
		e := <- mm.lchan
		switch e.Kind {
			case sktools.IK_SB_PRESS:
				pressed_on = e.Data				// presses are of no significance, so they are silently ignored, but we capture soft button info

			case sktools.IK_SB_RELEASE:
				if e.Data != pressed_on {		// if they pressed on one soft button, and moved off before release, ignore the release
					pressed_on = -1
					break
				}

				if e.Data > 1024 {				// not an anchor button -- percolate the interaction up to user
					b := mm.buttons[e.Data]
					if b != nil {
						me := &Mevent {
							Bid: e.Data,
						}
						switch b.kind {
							case sketch.BUTTON_STICKY: 			// sticky buttons change state with each release
								b.is_in	= !b.is_in
								me.Is_in = b.is_in

							case sketch.BUTTON_RADIO:			// must pop others out
								m := mm.b2menu[e.Data]					// but just radios in this menu
								for i := 0; i < len( m.buttons ); i++ {
									mb := m.buttons[i]
									if mb.kind == sketch.BUTTON_RADIO {
										m.gc.Release_button( m.name, mb.bid )
										mb.is_in = false
									}
								}
								mb := mm.buttons[e.Data]
								m.gc.Press_button( m.name, mb.bid )
								mb.is_in = true
						}

						b.uchan <- me
					} else {
						//fmt.Fprintf( os.Stderr, "soft button release: button not found: %d\n", e.Data )
					}
				} else {
					m := mm.a2menu[e.Data]
					if m != nil {
						m.toggle_visibility( )		
					} else {
						//fmt.Fprintf( os.Stderr, "soft button release for an anchor button: %d, but couldn't find menu\n", e.Data )
					}
				}

			case sktools.IK_M_PRESS:
				// presses are of no significance, so they are silently ignored

			case sktools.IK_M_RELEASE:
				//fmt.Fprintf( os.Stderr, "hard mouse button release: %d,%d %d\n", int( e.X ), int( e.Y ), e.Data )

			case sktools.IK_M_MOVED	:				// mouse movement (not supported at the moment)
				//fmt.Fprintf( os.Stderr, "unrecognised event kind: ignored: %d\n", e.Kind )
		}
	}
}

