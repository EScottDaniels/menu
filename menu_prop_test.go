/*
	Mnemonic:	menu_prop_test.go
	Abstract:	Unit test for menu properties.
	Author:		E. Scott Daniels
	Date:		16 December 2018
*/

package menu_test

import (
	"fmt"
	"os"
	"time"
	"testing"

	"bitbucket.org/EScottDaniels/sketch"
	"bitbucket.org/EScottDaniels/menu"
)

/*
	One instance of this function is started per menu that is created.
	This simplifies the code such that only the base of the button IDs
	is needed.
*/
func pm_event_leach( mm *menu.Menu_mgr, mname string, echan chan *menu.Mevent, base int, close_on_click bool, p *menu.Properties ) {
	for ;; {
		e := <- echan
		bid := e.Bid - base				// relative button in menu

		switch mname {
			case "control":
				switch bid {
					case 0:		mm.Hide_menu( mname )

					case 1:		mm.Display_menu( "properties" )

					case 2:
						for i := 1; i < 5; i++  {
							pname := fmt.Sprintf( "p%d", i )
							fmt.Fprintf( os.Stderr, "property %s set=%v\n", pname, p.Is_set( pname ) )
						}

					default:
							os.Exit( 0 )
				}
		}

		if close_on_click {
			mm.Hide_menu( mname )
		} else {
			if bid == 0 {
				mm.Hide_menu( mname )
			}
		}
	}
}


/*
*/
func Test_prop_menu(t *testing.T) {
	var (
		m1_strs string = " Close Menu:#a09000, Properties, Show Properties, Quit:#a00040"
		mp_json = `
		{	"defaults": { "colour": "#000080", "action": "set", "kind": "sticky" },
			"entries": [
				{ "label": "Property 1", "name": "p1" },
				{ "label": "Property 2", "name": "p2" },
				{ "label": "Property 3", "name": "p3", "init_state": true },
				{ "label": "Property 4", "name": "p4" },
				{ "label": "Property 5", "name": "p5" },
				{ "label": "Close", "name": "closer", "kind": "spring", "action": "close", "colour": "#a00040" }
			]
		}
		`
	)

	gc, err := sketch.Mk_graphics_api( "xi", ",900,1200,Menu Test")
	if err != nil {
		fmt.Fprintf(os.Stderr, "[FAIL] cannot create graphics conext: %s\n", err)
		t.Fail()
		return
	}
	fmt.Fprintf(os.Stderr, "[OK]  created graphics construct\n" );

	echan1 := make( chan *menu.Mevent, 128 )

	gc.Select_subpage( "root" )
	m := menu.Mk_menu_set( gc, 10, 10, 25, 55, "#0040a0", "white", false )

	base_m1, _ := build_menu( m, "control", "W1KW", m1_strs, echan1, true )

	pm := menu.Mk_prop_menu( m, "properties", 20, 40, 25, 100, mp_json )
	go pm_event_leach( m, "control", echan1, base_m1, true, pm )

	for ;; {
		gc.Show()
		gc.Drive()
		time.Sleep( 10 * time.Millisecond )
	}
}
